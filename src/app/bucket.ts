import { Location } from './location';

export class Bucket {
    id: string;
    name: string;
    location: Location;
    
    constructor(id: string, name: string, idLocation: string, nameLocation: string) {
        this.id = id;
        this.name = name;
        this.location = new Location(idLocation, nameLocation);
    }
}