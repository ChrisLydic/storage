export class Document {
    name: string;
    modified: string;
    size: number;
    
    constructor(name: string, modified: string, size: number){
        this.name = name;
        this.modified = modified;
        this.size = size;
    }
}