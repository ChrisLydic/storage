import { Component } from '@angular/core';

import { ErrorService } from './error.service';
 
@Component({
    selector: 'error-message',
    templateUrl: './error.component.html'
})
export class ErrorComponent {
    constructor(public errorService: ErrorService) {}
}