import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { BucketDetailComponent } from './bucket-detail.component';
import { BucketListComponent } from './bucket-list.component';

const routes: Routes = [
    { path: '', component: BucketListComponent },
    { path: 'bucket/:id', component: BucketDetailComponent },
];
 
@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}