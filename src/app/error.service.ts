import { Injectable } from '@angular/core';

/**
 * Used to log errors and to display a user friendly message.
 */
@Injectable()
export class ErrorService {
    errorMessage: string;

    constructor() { }

    set(message: string, error: Error) {
        console.error(error);
        this.errorMessage = message;
    }

    clear() {
        this.errorMessage = null;
    }
}
