import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BucketDetailComponent } from './bucket-detail.component';
import { BucketFormComponent } from './bucket-form.component';
import { BucketListComponent } from './bucket-list.component';
import { BytePipe } from './byte.pipe';
import { ErrorComponent } from './error.component';
import { ErrorService } from './error.service';
import { RouterLinkDirectiveStub } from './router-link-directive-stub';
import { StorageService } from './storage.service';

@NgModule({
    declarations: [
        AppComponent,
        BucketListComponent,
        BucketFormComponent,
        BucketDetailComponent,
        ErrorComponent,
        BytePipe,
        RouterLinkDirectiveStub
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        AppRoutingModule
    ],
    providers: [StorageService, ErrorService],
    bootstrap: [AppComponent]
})
export class AppModule { }
