import { catchError, map, tap, retry } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Bucket } from './bucket';
import { Document } from './document';
import { environment } from '../environments/environment';
import { ErrorService } from './error.service';
import { Location } from './location';

const options = { headers: { 'Authorization': 'Token ' + environment.secret } };

/**
 * Calls the 3fs storage API to get/create/delete buckets and their files.
 */
@Injectable()
export class StorageService {
    private url = 'https://challenge.3fs.si/storage';
    private locationsUrl = this.url + '/locations';
    private bucketsUrl = this.url + '/buckets';

    constructor(
        private http: HttpClient,
        private errorService: ErrorService) { }

    getLocations(): Observable<Location[]>  {
        return this.http.get<Location[]>(this.locationsUrl, options)
            .pipe(
                retry(3),
                map((data: any) => {
                    let locations: Location[] = [];
                    for (let i = 0; i < data.locations.length; i++) {
                        locations.push(new Location(data.locations[i].id, data.locations[i].name));
                    }
                    return locations;
                }),
                catchError((err: HttpErrorResponse) => {
                    this.errorService.set("Unable to retrieve locations.", err);
                    return of([]);
                })
            );
    }

    getBuckets(): Observable<Bucket[]> {
        return this.http.get<Bucket[]>(this.bucketsUrl, options)
            .pipe(
                retry(3),
                map((data: any) => {
                    let buckets: Bucket[] = [];
                    for (let i = 0; i < data.buckets.length; i++) {
                        buckets.push(
                            new Bucket(
                                data.buckets[i].id,
                                data.buckets[i].name,
                                data.buckets[i].location.id,
                                data.buckets[i].location.name
                            )
                        );
                    }
                    return buckets;
                }),
                catchError((err: HttpErrorResponse) => {
                    this.errorService.set("Unable to retrieve buckets.", err);
                    return of([]);
                })
            );
    }

    getBucket(bucket: string): Observable<Bucket> {
        let url = `${this.bucketsUrl}/${bucket}`;

        return this.http.get<Bucket>(url, options)
            .pipe(
                retry(3),
                map((data: any) => {
                    return new Bucket(
                        data.bucket.id,
                        data.bucket.name,
                        data.bucket.location.id,
                        data.bucket.location.name
                    );
                }),
                catchError((err: HttpErrorResponse) => {
                    this.errorService.set("Unable to retrieve bucket.", err);
                    return of(null);
                })
            );
    }

    createBucket(name: string, location: string): Observable<Bucket> {
        let data = {name: name, location: location};

        return this.http.post<Bucket>(this.bucketsUrl, data, options)
            .pipe(
                catchError((err: HttpErrorResponse) => {
                    this.errorService.set("Unable to create bucket.", err);
                    return of(null);
                })
            );
    }

    deleteBucket(bucket: string): Observable<boolean> {
        let url = `${this.bucketsUrl}/${bucket}`;

        return this.http.delete<any>(url, options)
            .pipe(
                retry(3),
                map(() => true),
                catchError((err: HttpErrorResponse) => {
                    this.errorService.set("Unable to delete bucket.", err);
                    return of(false);
                })
            );
    }

    getDocuments(bucket: string): Observable<Document[]> {
        let url = `${this.bucketsUrl}/${bucket}/objects`;

        return this.http.get<Document[]>(url, options)
            .pipe(
                retry(3),
                map((data: any) => {
                    let documents: Document[] = [];
                    for (let i = 0; i < data.objects.length; i++) {
                        documents.push(
                            new Document(
                                data.objects[i].name,
                                data.objects[i].last_modified,
                                data.objects[i].size
                            )
                        );
                    }
                    return documents;
                }),
                catchError((err: HttpErrorResponse) => {
                    this.errorService.set("Unable to retrieve files.", err);
                    return of([]);
                })
            );
    }

    uploadDocument(bucket: string, file: File): Observable<Document> {
        let url = `${this.bucketsUrl}/${bucket}/objects`;
        let formData: FormData = new FormData();
        formData.append('file', file, file.name);

        return this.http.post<Document>(url, formData, options)
            .pipe(
                map((data: any) => {
                    return new Document(data.name, data.last_modified, data.size);
                }),
                catchError((err: HttpErrorResponse) => {
                    if (err.status === 409 && err.error.message === `Object ${file.name} already exists`) {
                        this.errorService.set("File already exists.", err);
                    } else {
                        this.errorService.set("File upload failed.", err);
                    }
                    return of(null);
                })
            );
    }

    deleteDocument(bucket: string, name: string): Observable<boolean> {
        let url = `${this.bucketsUrl}/${bucket}/objects/${name}`;

        return this.http.delete<any>(url, options)
            .pipe(
                retry(3),
                map(() => true),
                catchError((err: HttpErrorResponse) => {
                    this.errorService.set("Unable to delete file.", err);
                    return of(false);
                })
            );
    }
}