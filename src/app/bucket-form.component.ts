import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { Bucket } from './bucket';
import { Location } from './location';
import { StorageService } from './storage.service';

/**
 * New bucket form. Creates buckets from user input and sends
 * the result to parent (bucket list component).
 */
@Component({
    selector: 'bucket-form',
    templateUrl: './bucket-form.component.html'
})
export class BucketFormComponent implements OnInit {
    @Output() created: EventEmitter<Bucket> = new EventEmitter<Bucket>();
    locations: Location[] = [];
    bucketName: string;
    locationId: string;

    constructor(private storageService: StorageService) {}

    ngOnInit(): void {
        this.storageService.getLocations().subscribe(locations => {
            this.locations = locations;
            // set selected location
            this.locationId = this.locations[0].id;
        });
    }

    onSubmit(): void {
        this.storageService.createBucket(this.bucketName, this.locationId)
            .subscribe(bucket => {
                if (bucket) {
                    // send the new bucket to the bucket list component
                    this.created.emit(bucket);

                    // reset form data for next time user opens the form
                    this.bucketName = "";
                    this.locationId = this.locations[0].id;
                }
            });
    }

}
