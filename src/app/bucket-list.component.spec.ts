import { async, TestBed } from '@angular/core/testing';
import { defer } from 'rxjs/observable/defer';
import { FormsModule } from '@angular/forms';

import { Bucket } from './bucket';
import { BucketListComponent } from './bucket-list.component';
import { BucketFormComponent } from './bucket-form.component';
import { Location } from './location';
import { RouterLinkDirectiveStub } from './router-link-directive-stub';
import { StorageService } from './storage.service';

describe('BucketListComponent', () => {
    let storageService: jasmine.SpyObj<StorageService>;
    let buckets: Bucket[];

    beforeEach(async(() => {
        const storageServiceSpy = jasmine.createSpyObj('StorageService', ['getLocations', 'getBuckets']);

        TestBed.configureTestingModule({
            declarations: [
                BucketListComponent,
                BucketFormComponent,
                RouterLinkDirectiveStub
            ],
            imports: [
                FormsModule
            ],
            providers: [
                { provide: StorageService, useValue: storageServiceSpy }
            ]
        }).compileComponents();

        storageService = TestBed.get(StorageService);

        // create data needed when component loads

        const locations = [
            new Location('571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4', 'Ljubljana'),
            new Location('541909F3-20FC-4382-A8E8-18042F5E7677', 'Kranj')
        ];
        storageService.getLocations.and.returnValue(defer(() => Promise.resolve(locations)));

        buckets = [
            new Bucket('another-bucket', 'Another Bucket', '571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4', 'Ljubljana'),
            new Bucket('data', 'Data', '541909F3-20FC-4382-A8E8-18042F5E7677', 'Kranj')
        ];
        storageService.getBuckets.and.returnValue(defer(() => Promise.resolve(buckets)));
    }));

    it('should create the bucket list component', async(() => {
        const fixture = TestBed.createComponent(BucketListComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    }));
    
    it(`should load list of buckets`, async(() => {
        const fixture = TestBed.createComponent(BucketListComponent);
        
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            const component = fixture.debugElement.componentInstance;
            expect(component.buckets).toEqual(buckets, 'expected buckets');
        });
    }));
    
    it('should render list of buckets', async(() => {
        const fixture = TestBed.createComponent(BucketListComponent);
        
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            const compiled = fixture.debugElement.nativeElement;
            expect(compiled.querySelector('.table tr td').textContent).toContain('Another Bucket', "expected first bucket's name");
        });
    }));

    it('should not render create bucket form', async(() => {
        const fixture = TestBed.createComponent(BucketListComponent);
        
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            const compiled = fixture.debugElement.nativeElement;
            expect(compiled.querySelector('bucket-form').hasAttribute('hidden')).toEqual(true, 'expected hidden form');
        });
    }));
    
    it('should render create bucket form', async(() => {
        const fixture = TestBed.createComponent(BucketListComponent);
        
        fixture.componentInstance.formHidden = false;

        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            const compiled = fixture.debugElement.nativeElement;
            expect(compiled.querySelector('bucket-form').hasAttribute('hidden')).toEqual(false, 'expected visible form');
        });
    }));
});
