import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { defer } from 'rxjs/observable/defer';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';

import { Bucket } from './bucket';
import { Document } from './document';
import { ErrorService } from './error.service';
import { Location } from './location';
import { StorageService } from './storage.service';


describe('StorageService', () => {
    let service: StorageService;
    let httpSpy: jasmine.SpyObj<HttpClient>;
    let errorSpy: jasmine.SpyObj<ErrorService>;

    beforeEach(() => {
        const httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete']);
        const errorServiceSpy = jasmine.createSpyObj('ErrorService', ['set']);

        TestBed.configureTestingModule({
            providers: [
                StorageService,
                { provide: HttpClient, useValue: httpClientSpy },
                { provide: ErrorService, useValue: errorServiceSpy }
            ]
        });
        
        service = TestBed.get(StorageService);
        httpSpy = TestBed.get(HttpClient);
        errorSpy = TestBed.get(ErrorService);
    });

    it('should return expected locations', () => {
        const data = { locations: [
            { id: '571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4', name: 'Ljubljana' },
            { id: '541909F3-20FC-4382-A8E8-18042F5E7677', name:'Kranj' }
        ] };

        const expectedLocations = [
            new Location('571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4', 'Ljubljana'),
            new Location('541909F3-20FC-4382-A8E8-18042F5E7677', 'Kranj')
        ];
        
        httpSpy.get.and.returnValue(defer(() => Promise.resolve(data)));

        service.getLocations().subscribe(
            locations => expect(locations).toEqual(expectedLocations, 'expected locations'),
            fail
        );

        expect(httpSpy.get.calls.count()).toBe(1, 'one call');
    });

    it('should return expected buckets', () => {
        const data = { buckets: [
            { id: 'another-bucket', name: 'Another Bucket', location: { id: '571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4', name: 'Ljubljana' } },
            { id: 'data', name: 'Data', location: { id: '541909F3-20FC-4382-A8E8-18042F5E7677', name:'Kranj' } }
        ] };

        const expectedBuckets = [
            new Bucket('another-bucket', 'Another Bucket', '571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4', 'Ljubljana'),
            new Bucket('data', 'Data', '541909F3-20FC-4382-A8E8-18042F5E7677', 'Kranj')
        ];
        
        httpSpy.get.and.returnValue(defer(() => Promise.resolve(data)));

        service.getBuckets().subscribe(
            buckets => expect(buckets).toEqual(expectedBuckets, 'expected buckets'),
            fail
        );

        expect(httpSpy.get.calls.count()).toBe(1, 'one call');
    });
    
    it('should return expected bucket', () => {
        const data = { bucket: { id: 'another-bucket', name: 'Another Bucket', location: { id: '571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4', name: 'Ljubljana' } } };
        const expectedBucket = new Bucket('another-bucket', 'Another Bucket', '571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4', 'Ljubljana');
        
        httpSpy.get.and.returnValue(defer(() => Promise.resolve(data)));

        service.getBucket('bucket-id').subscribe(
            bucket => expect(bucket).toEqual(expectedBucket, 'expected bucket'),
            fail
        );

        expect(httpSpy.get.calls.count()).toBe(1, 'one call');
    });

    it('should return expected documents', () => {
        const data = { objects: [
            { name: 'loremipsum.pdf', size: 77123, last_modified: '2018-04-08T02:06:51.318459725Z' },
            { name: 'lydic_resume.pdf', size: 96487, last_modified: '2018-04-07T02:48:14.021805124Z' },
            { name: 'sherlock holmes.txt', size: 257693, last_modified: '2018-04-07T17:23:41.181307681Z' }
        ] };

        const expectedDocuments = [
            new Document('loremipsum.pdf', '2018-04-08T02:06:51.318459725Z', 77123),
            new Document('lydic_resume.pdf', '2018-04-07T02:48:14.021805124Z', 96487),
            new Document('sherlock holmes.txt', '2018-04-07T17:23:41.181307681Z', 257693)
        ];
        
        httpSpy.get.and.returnValue(defer(() => Promise.resolve(data)));

        service.getDocuments('another-bucket').subscribe(
            documents => expect(documents).toEqual(expectedDocuments, 'expected buckets'),
            fail
        );

        expect(httpSpy.get.calls.count()).toBe(1, 'one call');
    });

    it('should return uploaded document data', () => {
        const data = { name: 'BACKGROUNDS_01.png', size: 324673, last_modified: '2018-04-08T20:11:12.862921962Z' };
        const expectedDocument = new Document('BACKGROUNDS_01.png', '2018-04-08T20:11:12.862921962Z', 324673);
        const file = new File(['test'], 'beowulf.txt');
        
        httpSpy.post.and.returnValue(defer(() => Promise.resolve(data)));

        service.uploadDocument('another-bucket', file).subscribe(
            document => {
                expect(document).toEqual(expectedDocument, 'expected a document');
            }
        );

        expect(httpSpy.post.calls.count()).toBe(1, 'one call');
    });
    
    it('should return error when the server returns a 404 instead of buckets', () => {
        const errorResponse = new HttpErrorResponse({
            error: '404 error message',
            status: 404
        });
        
        httpSpy.get.and.returnValue(defer(() => Promise.reject(errorResponse)));
        errorSpy.set.and.callFake(function(){});
        
        service.getBuckets().subscribe(
            buckets => {
                expect(buckets).toEqual([], 'expected an error, not bucket list');
                expect(errorSpy.set).toHaveBeenCalledWith('Unable to retrieve buckets.', errorResponse);
            }
        );
    });

    it('should return error when the server returns a 404 instead of the bucket', () => {
        const errorResponse = new HttpErrorResponse({
            error: '404 error message',
            status: 404
        });
        
        httpSpy.get.and.returnValue(defer(() => Promise.reject(errorResponse)));
        errorSpy.set.and.callFake(function(){});
        
        service.getBucket('another-bucket').subscribe(
            bucket => {
                expect(bucket).toEqual(null, 'expected an error, not a bucket');
                expect(errorSpy.set).toHaveBeenCalledWith('Unable to retrieve bucket.', errorResponse);
            }
        );
    });

    it('should return error when the server returns a 404 instead of locations', () => {
        const errorResponse = new HttpErrorResponse({
            error: '404 error message',
            status: 404
        });
        
        httpSpy.get.and.returnValue(defer(() => Promise.reject(errorResponse)));
        errorSpy.set.and.callFake(function(){});
        
        service.getLocations().subscribe(
            locations => {
                expect(locations).toEqual([], 'expected an error, not locations');
                expect(errorSpy.set).toHaveBeenCalledWith('Unable to retrieve locations.', errorResponse);
            }
        );
    });

    it('should return error when the server returns a 404 instead of documents', () => {
        const errorResponse = new HttpErrorResponse({
            error: '404 error message',
            status: 404
        });
        
        httpSpy.get.and.returnValue(defer(() => Promise.reject(errorResponse)));
        errorSpy.set.and.callFake(function(){});
        
        service.getDocuments('another-bucket').subscribe(
            documents => {
                expect(documents).toEqual([], 'expected an error, not documents');
                expect(errorSpy.set).toHaveBeenCalledWith('Unable to retrieve files.', errorResponse);
            }
        );
    });

    it('should return correct error when a duplicate file is uploaded', () => {
        const errorResponse = new HttpErrorResponse({
            error: { message: 'Object beowulf.txt already exists' },
            status: 409
        });
        const file = new File(['test'], 'beowulf.txt');
        
        httpSpy.post.and.returnValue(defer(() => Promise.reject(errorResponse)));
        
        service.uploadDocument('another-bucket', file).subscribe(
            result => {
                expect(result).toEqual(null, 'expected an error, not a document');
                expect(errorSpy.set).toHaveBeenCalledWith('File already exists.', errorResponse);
            }
        );
    });

    it('should return error when server returns 500 instead of uploaded document', () => {
        const errorResponse = new HttpErrorResponse({
            error: { message: 'Unknown error occurred' },
            status: 500
        });
        const file = new File(['test'], 'beowulf.txt');
        
        httpSpy.post.and.returnValue(defer(() => Promise.reject(errorResponse)));
        
        service.uploadDocument('another-bucket', file).subscribe(
            result => {
                expect(result).toEqual(null, 'expected an error, not a document');
                expect(errorSpy.set).toHaveBeenCalledWith('File upload failed.', errorResponse);
            }
        );
    });

    it('should return error when server returns 500 instead of deleting bucket', () => {
        const errorResponse = new HttpErrorResponse({
            error: { message: 'Unknown error occurred' },
            status: 500
        });
        
        httpSpy.delete.and.returnValue(defer(() => Promise.reject(errorResponse)));
        
        service.deleteBucket('another-bucket').subscribe(
            result => {
                expect(result).toEqual(false, 'expected an error, not a document');
                expect(errorSpy.set).toHaveBeenCalledWith('Unable to delete bucket.', errorResponse);
            }
        );
    });

    it('should return error when server returns 500 instead of deleting document', () => {
        const errorResponse = new HttpErrorResponse({
            error: { message: 'Unknown error occurred' },
            status: 500
        });
        
        httpSpy.delete.and.returnValue(defer(() => Promise.reject(errorResponse)));
        
        service.deleteDocument('another-bucket', 'beowulf.txt').subscribe(
            result => {
                expect(result).toEqual(false, 'expected an error, not a document');
                expect(errorSpy.set).toHaveBeenCalledWith('Unable to delete file.', errorResponse);
            }
        );
    });

});