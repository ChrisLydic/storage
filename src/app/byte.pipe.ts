import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'byte'})
export class BytePipe implements PipeTransform {
    transform(value: number): string {
        let formatted = "";

        if (value >= 1000000000) {
            value /= 1000000000;
            formatted = Math.round(value) + "GB";
        } else if (value >= 1000000) {
            value /= 1000000;
            formatted = Math.round(value) + "MB";
        } else if (value >= 1000) {
            value /= 1000;
            formatted = Math.round(value) + "KB";
        } else {
            formatted = Math.round(value) + "B";
        }

        return formatted;
    }
}