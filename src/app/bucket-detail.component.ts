import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Bucket } from './bucket';
import { Document } from './document';
import { StorageService } from './storage.service';

/**
 * For accessing the documents within a bucket,
 * along with some of the bucket's statistics.
 */
@Component({
    selector: 'bucket-detail',
    templateUrl: './bucket-detail.component.html',
    styleUrls: ['./bucket-detail.component.css']
})
export class BucketDetailComponent implements OnInit {
    documents: Document[];
    bucket: Bucket;
    // for switching between the two tabs
    viewFiles: boolean = true;
    currentDocument: string;
    fileSize: number = 0;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private storageService: StorageService) {}

    ngOnInit(): void {
        const id: string = this.route.snapshot.paramMap.get('id');

        this.storageService.getBucket(id)
            .subscribe(bucket => {
                if (bucket) {
                    this.bucket = bucket;
                    this.getDocuments();
                } else {
                    this.documents = [];
                }
            });
    }

    selectDocument(name): void {
        this.currentDocument = name;
    }

    deleteDocument(): void {
        if (this.currentDocument) {
            if (confirm("Do you really want to delete this object?")) {
                this.storageService.deleteDocument(this.bucket.id, this.currentDocument)
                    .subscribe(result => {
                        if (result) {
                            this.documents = this.documents
                                .filter(doc => doc.name !== this.currentDocument);
                        }
                    });
            }
        }
    }

    upload(event): void {
        let files: FileList = event.target.files;

        if (files.length > 0) {
            let newFile: File = files[0];
            this.storageService.uploadDocument(this.bucket.id, newFile)
                .subscribe(document => {
                    if (document) {
                        this.documents.push(document);
                    }
                });
        }
    }

    toggleViewFiles(): void {
        this.viewFiles = !this.viewFiles;

        if (!this.viewFiles && this.documents) {
            this.fileSize = this.documents.reduce(
                (prev: number, data: Document) => prev + data.size,
                0
            );
        }
    }

    deleteBucket(): void {
        if (confirm("Do you really want to delete this bucket?")) {
            this.storageService.deleteBucket(this.bucket.id)
                .subscribe(result => {
                    if (result) {
                        this.router.navigate(['/']);
                    }
                });
        }
    }

    private getDocuments(): void {
        this.storageService.getDocuments(this.bucket.id).subscribe(documents => {
            this.documents = documents;
        });
    }
}
