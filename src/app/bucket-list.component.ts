import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { Bucket } from './bucket';
import { StorageService } from './storage.service';

/**
 * Displays a list of buckets and contains the create bucket form component.
 */
@Component({
    selector: 'bucket-list',
    templateUrl: './bucket-list.component.html',
    styleUrls: []
})
export class BucketListComponent implements OnInit {
    buckets: Bucket[];
    formHidden: boolean = true;

    constructor(private storageService: StorageService) {}

    ngOnInit() {
        this.storageService.getBuckets()
            .subscribe(buckets => this.buckets = buckets);
    }

    toggleForm(): void {
        this.formHidden = !this.formHidden;
    }

    onNewBucket(created: Bucket): void {
        this.buckets.push(created);
        this.toggleForm();
    }
}
