import { async, TestBed } from '@angular/core/testing';
import { defer } from 'rxjs/observable/defer';
import { FormsModule } from '@angular/forms';

import { Bucket } from './bucket';
import { BucketFormComponent } from './bucket-form.component';
import { StorageService } from './storage.service';
import { Location } from './location';

describe('BucketFormComponent', () => {
    let storageService: jasmine.SpyObj<StorageService>;

    beforeEach(async(() => {
        const storageServiceSpy = jasmine.createSpyObj('StorageService', ['getLocations', 'createBucket']);

        TestBed.configureTestingModule({
            declarations: [
                BucketFormComponent
            ],
            imports: [
                FormsModule
            ],
            providers: [
                { provide: StorageService, useValue: storageServiceSpy }
            ]
        }).compileComponents();

        storageService = TestBed.get(StorageService);

        // create data needed when component loads

        const locations = [
            new Location('571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4', 'Ljubljana'),
            new Location('541909F3-20FC-4382-A8E8-18042F5E7677', 'Kranj')
        ];
        storageService.getLocations.and.returnValue(defer(() => Promise.resolve(locations)));
    }));

    it('should create the bucket form component', async(() => {
        const fixture = TestBed.createComponent(BucketFormComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    }));

    it('should return the created bucket on form submit', async(() => {
        const fixture = TestBed.createComponent(BucketFormComponent);
        const comp = fixture.componentInstance;
        const bucket: Bucket = new Bucket('another-bucket', 'Another Bucket', '571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4', 'Ljubljana');

        storageService.createBucket.and.returnValue(defer(() => Promise.resolve(bucket)));
        
        comp.bucketName = bucket.name;
        comp.locationId = bucket.location.id;

        fixture.detectChanges();

        comp.created.subscribe(newBucket => expect(newBucket).toBe(bucket, 'expected bucket'));
        comp.onSubmit();
    }));
});
