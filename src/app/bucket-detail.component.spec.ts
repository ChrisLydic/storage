import { async, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { defer } from 'rxjs/observable/defer';

import { BucketDetailComponent } from './bucket-detail.component';
import { BytePipe } from './byte.pipe';
import { Bucket } from './bucket';
import { Document } from './document';
import { StorageService } from './storage.service';

describe('BucketDetailComponent', () => {
    let storageService: jasmine.SpyObj<StorageService>;
    let documents;

    beforeEach(async(() => {
        const storageServiceSpy = jasmine.createSpyObj('StorageService', ['getBucket', 'getDocuments']);
        const activatedRouteStub = {snapshot: {paramMap: {get: () => 'another-bucket'}}};
        const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

        TestBed.configureTestingModule({
            declarations: [
                BucketDetailComponent,
                BytePipe
            ],
            providers: [
                { provide: StorageService, useValue: storageServiceSpy },
                { provide: Router, useValue: routerSpy },
                { provide: ActivatedRoute, useValue: activatedRouteStub }
            ]
        }).compileComponents();

        storageService = TestBed.get(StorageService);

        // create data needed when component loads

        const bucket = new Bucket('another-bucket', 'Another Bucket', '571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4', 'Ljubljana');
        storageService.getBucket.and.returnValue(defer(() => Promise.resolve(bucket)));
        
        documents = [
            new Document('loremipsum.pdf', '2018-04-08T02:06:51.318459725Z', 77123),
            new Document('lydic_resume.pdf', '2018-04-07T02:48:14.021805124Z', 96487),
            new Document('sherlock holmes.txt', '2018-04-07T17:23:41.181307681Z', 257693)
        ];
        storageService.getDocuments.and.returnValue(defer(() => Promise.resolve(documents)));
    }));

    it('should create the bucket detail component', async(() => {
        const fixture = TestBed.createComponent(BucketDetailComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    }));
    
    it(`should load list of documents`, async(() => {
        const fixture = TestBed.createComponent(BucketDetailComponent);
        
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            const component = fixture.debugElement.componentInstance;
            expect(component.documents).toEqual(documents, 'expected documents');
        });
    }));
    
    it('should render list of documents', async(() => {
        const fixture = TestBed.createComponent(BucketDetailComponent);
        
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            const compiled = fixture.debugElement.nativeElement;
            expect(compiled.querySelector('.table tr td').textContent).toContain('loremipsum.pdf', "expected first file's name");
        });
    }));

    it('should not render bucket details', async(() => {
        const fixture = TestBed.createComponent(BucketDetailComponent);
        
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            const compiled = fixture.debugElement.nativeElement;
            expect(compiled.querySelector('article:nth-of-type(2)').hasAttribute('hidden')).toEqual(true, 'expected hidden article');
        });
    }));
    
    it('should render bucket details', async(() => {
        const fixture = TestBed.createComponent(BucketDetailComponent);
        
        fixture.componentInstance.viewFiles = false;

        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            const compiled = fixture.debugElement.nativeElement;
            expect(compiled.querySelector('article:nth-of-type(2)').hasAttribute('hidden')).toEqual(false, 'expected visible article');
        });
    }));
});
