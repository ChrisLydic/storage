# Storage

The API key is stored in the environment files, which are in the gitignore.
To use your key, add a 'secret: your-key' field to your angular environment files in the environments folder.

To add a new environment file, create an environment.ts file in the src/environments folder.
```javascript
export const environment = {
    production: false,
    secret: '00000000000000'
};
```

Run `npm install` to install dependencies.

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `npm run build` to build the production build of the project. The build artifacts will be stored in the `dist/` directory.

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).